from parameters import report_filename
from parameters import channels
from parameters import rec_folders
from parameters import batch_size
from parameters import epochs as train_epochs
from parameters import context_left
from parameters import context_right

from src.reporting import PDFManager
from src.reporting import plot_confusion_matrix

from src.models import build_network
from src.models import train_network
from src.models import network_classify
from src.dataset import open_recording

import gc
from statistics import mean
from functools import reduce
from random import sample
from random import uniform
from random import choices

import matplotlib.pyplot as plt
import sklearn.metrics
from tqdm import tqdm
import numpy as np
from torch import optim
from torch import nn
import torch

import time

import concurrent.futures

def apply_noise(X, factor=1.0):
    X = np.array(X)
    noise = (np.random.random(X.shape) * 2.0 - 1.0) * factor
    return X + noise

def apply_destruction(X, factor=0.1):
    """Applies destruction to a contigous region of X.

    factor: fraction of X to be destroyed."""
    X = np.array(X)
    i = int(uniform(0,1-factor) * len(X))
    j = int((i + factor) * len(X))
    prev_shape = X.shape
    X = np.concatenate((X[:i], np.zeros_like(X[i:j]), X[j:]))
    assert X.shape == prev_shape
    return X

def leave_one_out(rec_folders,
        report_pdf,
        train_epochs,
        batch_size,
        destruction = 0.0,
        noise = 0.0,
        augmentation = 0.0,
        ):
    """
    rec_folders : list with recording folders

    augmentation : fraction of the dataset to be used with replacement in
    each data augmentation task

    noise : noise factor

    destruction: fraction (of a given signal) to be removed in data augmentation

    """
    experiment_name = "Leave-one-out\ndestruction={} noise={} augmentation={}".format(destruction,noise, augmentation)

    # Build list of confusion matrices
    confusion_matrices = list()

    # Leave i out
    avgacc = 0.0
    for i in tqdm(range(len(rec_folders)),desc="Leave-one-out test"):
        test_folders = [rec_folders[i]]
        train_folders = rec_folders[:i] + rec_folders[i+1:]

        # Build train dataset using hypnogram 0
        x_train, y_train = list(), list()
        for folder in tqdm(train_folders,desc="Opening train dataset"):
            epochs, hypnograms, _ = open_recording(folder, channels)

            # Contextualize
            if context_left > 0:
                epochs = np.concatenate([[np.zeros_like(epochs[0])]*context_left, epochs], axis=0)
            if context_right > 0:
                epochs = np.concatenate([epochs, [np.zeros_like(epochs[0])]*context_right], axis=0)

            # Generate dataset
            folder_x = list()
            folder_y = list()
            for i in range(len(hypnograms[0])):
                ix = i+context_left
                xi = [epochs[j] for j in range(ix-context_left, ix+context_right+1)]
                xi = np.concatenate(xi,axis=0)
                yi = hypnograms[0][i]

                folder_x.append(xi)
                folder_y.append(yi)

            x_train.append(np.array(folder_x))
            y_train.append(np.array(folder_y))

            del epochs
            del hypnograms

        classes_n = len(set((yi for yj in y_train for yi in yj)))
        augmentation_n = int(augmentation*len(x_train))

        # Add sub sequences for training.
        if augmentation > 0:
            for i,(x,y) in tqdm(list(enumerate(
                    choices(list(zip(x_train,y_train)),k=augmentation_n)
                    )),desc="Augmenting data with subsequences"):
                length = int(uniform(2, len(x)))
                start = int(uniform(0,len(x)-length))
                end = start + length
                x_train.append(x[start:end])
                y_train.append(y[start:end])

        # Add noise
        if noise > 0:
            for i,(x,y) in tqdm(enumerate(
                    choices(list(zip(x_train,y_train)),k=augmentation_n)
                    ),desc="Augmenting data with noise"):
                x_train.append(apply_noise(x, factor = noise))
                y_train.append(y)

        ## Add partially destroy samples
        if destruction > 0:
            for i,(x,y) in tqdm(enumerate(
                    choices(list(zip(x_train,y_train)),k=augmentation_n)
                    ), desc="Augmenting data with destruction"):
                x_train.append(apply_destruction(x, factor = destruction))
                y_train.append(y)

        x_shape = x_train[0][0].shape
        y_shape = y_train[0][0].shape

        # Build network
        network = build_network(x_shape, classes_n)
        n_trainable_params = sum(p.numel() for p in network.parameters() if p.requires_grad)
        for _ in tqdm(range(n_trainable_params),desc="Counting network parameters"):
            continue

        # Optimizer
        optimizer = optim.Adam(network.parameters(), lr=0.01)

        # Learning Rate scheduler
        switch_value = 1/100
        lr_lambda = [
                lambda epoch: 1.0
            ]

        scheduler = optim.lr_scheduler.LambdaLR(optimizer, lr_lambda)

        # Loss
        flat_y_train = [yi for yj in y_train for yi in yj]
        weight = [ flat_y_train.count(y) for y in set(flat_y_train) ]
        weight = [ max(weight)/yc for yc in weight ]
        weight = [ w for w in weight ]
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        weight = torch.Tensor(weight).to(device)
        loss_func = nn.CrossEntropyLoss(weight=weight)

        # Report network architecture details
        report_pdf.add_strings([
            "Architeture details:\n{}".format(str(network)),
            "Trainable paremeters:\n{}".format(str(n_trainable_params)),
            "Optimizer:\n{}".format(str(optimizer)),
            "Loss:\n{}".format(str(loss_func)),
            "Weight:\n{}".format(str(weight)),
            "LR scheduler:\n{}".format(str(scheduler)),
            ])

        # Train network
        training_time = time.time()
        network, losses = train_network(
                x_train,
                y_train,
                network,
                optimizer,
                scheduler,
                loss_func,
                train_epochs,
                batch_size,
            )
        network.eval()
        training_time = time.time() - training_time

        del x_train
        del y_train

        # Report network training details
        f, ax = plt.subplots()
        ax.set_title("Loss vs epoch\nTraining took {}s".format(round(training_time)))
        ax.plot(range(len(losses)),losses)
        report_pdf.add_fig(f)

        # Build encoding and projection function
        proj_enc = lambda x: project(encode(x, network, batch_size), hdencoder)
        couple = lambda l: sum(l)

        # Test on each recording
        avgacc, curr = 0,1
        with tqdm(test_folders, desc="Testing") as test_bar:
            for folder in test_bar:
                epochs, hypnograms, _ = open_recording(folder, channels)
                nepochs = list()
                if context_left > 0:
                    epochs = np.concatenate([[np.empty_like(epochs[0])]*context_left, epochs], axis=0)
                if context_right > 0:
                    epochs = np.concatenate([epochs, [np.empty_like(epochs[0])]*context_right], axis=0)
                for i in range(len(hypnograms[0])):
                    ix = i+context_left
                    xi = [epochs[j] for j in range(ix-context_left, ix+context_right+1)]
                    xi = np.concatenate(xi,axis=0)
                    nepochs.append(xi)
                epochs = nepochs

                if noise > 0:
                    for i,x in enumerate(epochs):
                        epochs[i] = apply_noise(x, factor = noise)

                # Classify using representatives
                y_pred = network_classify(epochs, network, batch_size)
                y_true = hypnograms[0]
                del epochs

                # Build confusion matrix
                confusion_matrix = sklearn.metrics.confusion_matrix(y_pred, y_true)
                confusion_matrices.append(confusion_matrix)

                # Plot confusion matrix
                title = experiment_name + str(folder)
                classes = list(map(str,set(y_true)|set(y_pred)))
                report_pdf.add_fig(plot_confusion_matrix(
                    confusion_matrix,
                    classes,
                    title=title+"\n{}".format(curr),
                    normalize=True,
                    ))

                # Evaluate
                evaluate = lambda h0,h1: mean((int(h0i==h1i) for h0i,h1i in zip(h0,h1)))
                model_acc = evaluate(y_pred, y_true)
                expert_acc = evaluate(hypnograms[1], y_true)

                # Plot hypnograms
                dic = {experiment_name+" acc={}".format(model_acc) : y_pred}
                dic["Expert 0"] = hypnograms[0]
                dic["Expert 1 acc={}".format(expert_acc)] = hypnograms[1]
                f, axs = plt.subplots(nrows=len(dic.items()), squeeze=False)
                axs = axs.flatten()
                for i,(label, values) in enumerate(dic.items()):
                    axs[i].set_title(label)
                    axs[i].plot(values)
                report_pdf.add_fig(f)

                avgacc = (avgacc*(curr-1) + model_acc)/curr
                curr += 1
                test_bar.set_postfix(avgacc=avgacc)

    # Plot final confusion matrix
    if len(confusion_matrices) > 0:
        mean_confusion_matrix = sum(confusion_matrices)/len(confusion_matrices)
        title = experiment_name + "\nMean confusion matrix\navgacc={}".format(avgacc)
        report_pdf.add_fig(plot_confusion_matrix(mean_confusion_matrix,classes,title=title))

    return avgacc

def main():
    # Set up report PDF
    report_pdf = PDFManager(report_filename)

    # Perform leave-one-out for different noise levels
    plot = list()
    with tqdm(range(0,10,1),total=10,desc="Leave-one-out train on noise") as pbar:
        for i in pbar:
            acc = leave_one_out(
                    rec_folders,
                    report_pdf,
                    train_epochs,
                    batch_size,
                    noise = i/100,
                    destruction = 0.00,
                    augmentation = 1.00,
                )
            plot.append((i, acc))
            pbar.set_postfix(destruction=i, last_avgacc=acc)
    f, ax = plt.subplots()
    ax.set_title("Accuracy vs noise")
    ax.plot([xy[0] for xy in plot], [xy[1] for xy in plot])
    report_pdf.add_fig(f)

if __name__ == "__main__":
    print("Read parameters.py to review script settings.")
    print("Read main.py for a process overview.")
    print("Read src/models.py to review model specific code.")
    main()
