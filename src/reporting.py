"""This file defines operations to build a PDF report file."""
# External imports
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import Figure

# Python imports
from typing import List, Dict
from threading import RLock
from os import remove, rename
from os.path import isfile

from pdfrw import PdfReader, PdfWriter

class PDFManager:
    """Thread safe representation of a PDF file"""
    def __init__(self, pdf_dest : str):
        self.pdf_dest = str(pdf_dest)
        self.lock = RLock()

    def add_pdf(self, pdf : str, bookmark : str = None):
        """Appends pdf to pdf_dest. If the file
        did not exist it is created."""
        with self.lock:
            filename3 = "./temp3.pdf"
            writer = PdfWriter()
            if isfile(self.pdf_dest):
                writer.addpages(PdfReader(self.pdf_dest).pages)
            writer.addpages(PdfReader(pdf).pages)
            writer.write(filename3)
            rename(filename3, self.pdf_dest)

    def add_fig(self, f : Figure, show : bool = False):
        """Convinience function. Adds the plot f to the pdf"""
        with self.lock:
            fig_pdf = "./fig.pdf"
            f.tight_layout()
            f.savefig(fname=fig_pdf,format='pdf')
            f.clear()
            plt.close(f)
            self.add_pdf(fig_pdf)
            remove(fig_pdf)

    def add_strings(self, strings):
        """Write a list of strings to filename in PDF format"""
        with self.lock:
            filename3 = "./strings.pdf"
            from fpdf import FPDF
            pdf = FPDF()
            pdf.add_page()
            pdf.set_font('Arial', '', 12)
            for string in strings:
                pdf.multi_cell(0, 5, string)
            pdf.output(filename3, 'F')
            self.add_pdf(filename3)
            remove(filename3)

import matplotlib
def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-90, ha="right")

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["black", "white"],
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A list or array of two color specifications.  The first is used for
        values below a threshold, the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts

def plot_matrix(matrix, x_ticks, y_ticks, title, cmap=plt.cm.Blues):
    f, ax = plt.subplots()
    ax.set_title(title)
    im, cbar = heatmap(matrix, x_ticks, y_ticks, ax=ax,
                   cmap=cmap)
    texts = annotate_heatmap(im, valfmt="{x:.2f}")
    return f

def plot_confusion_matrix(cm,
                          classes,
                          normalize=True,
                          title=str(),
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    # Compute confusion matrix
    norm = cm.sum(axis=1)[:, np.newaxis]
    for idx,val in np.ndenumerate(norm):
        if val == 0: norm[idx] = 1
    if normalize:
        cm = cm.astype('float') / norm

    xticks=["{} (pred)".format(c) for c in classes]
    yticks=["{} (true)".format(c) for c in classes]
    f = plot_matrix(cm, xticks, yticks, title, cmap=cmap)
    return f
