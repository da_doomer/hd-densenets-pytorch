"""Defines the function to open ISRUC database folders"""
import pyedflib
from glob import glob
import numpy as np
from sklearn.preprocessing import minmax_scale
import sklearn.preprocessing
from scipy.interpolate import CubicSpline
from pathlib import Path

def resample(signal, target_len):
    """Interpolates and samples signal to resize to target_len"""
    if len(signal) == target_len: return signal
    xold = minmax_scale(range(len(signal)), feature_range=(0,1))
    xnew = minmax_scale(range(target_len), feature_range=(0,1))
    yold = signal
    ynew = CubicSpline(xold, yold)(xnew)
    return ynew

def workf(li): return li[0],li[1],li[2](li[3])
def open_recording(rec_folder, channels):
    """Opens rec_folder from the ISRUC database

    epochs[i][j] is the jth signal of the ith epoch.

    Returns epochs[][], hypnograms, sample_rate"""
    # Read channels
    rec_file = glob(rec_folder + "/*.rec")[0]
    signals_raw    = list()
    sample_rates   = list()
    signals_labels = list()
    with pyedflib.EdfReader(rec_file) as f:
        heads = f.getSignalHeaders()
        labels = [header["label"] for header in heads]
        rates = [header["sample_rate"] for header in heads]
        work = list()
        for i,label in enumerate(labels):
            if label in channels:
                work.append((label,rates[i],f.readSignal,i))
        for l,r,s in map(workf,work):
            signals_raw.append(s)
            signals_labels.append(l)
            sample_rates.append(r)

    # Check if signals were read
    if len(sample_rates) == 0:
        print("Warning! Record {} has labels {} but none of the requested {}".format(
            rec_folder,str(labels),str(channels)))
        return [],[],0

    # Resample signals to the lowest sample_rate
    target_len = min([len(signal) for signal in signals_raw])
    sample_rate = min(sample_rates)
    if len(set(sample_rates)) > 1: print("Resampling to {}Hz!".format(sample_rate))
    signals = list()
    for i,signal in enumerate(signals_raw):
        if len(signal) != target_len:
            print("Resampling {} from {}Hz to {}Hz".format(
                  signals_labels[i],sample_rates[i],sample_rate))
            signal = resample(signal, target_len)
        signals.append(signal)

    # -1,1-normalize each signal
    for i,signal in enumerate(signals):
        min_n = min(signal)
        max_n = max(signal)
        for j,v in enumerate(signal):
            if v >= 0: signal[j] = v/max_n
            else: signal[j] = v/(min_n*-1)

    # Read hypnograms
    hypno_files = Path(rec_folder).glob("*.txt")
    hypnograms = list()
    for hypno_file in hypno_files:
        with open(hypno_file) as h:
            h = [line for line in h if len(line.strip()) > 0 ]
        h = list(map(int,h))
        fixer = lambda n: 4 if n == 5 else n
        h = [fixer(n) for n in h]
        hypnogram = h
        hypnograms.append(hypnogram)
    hypnograms = [h for h in hypnograms if len(h) == len(hypnograms[0])]

    # Segment into 30s epochs
    samples = np.array(signals).T
    w = sample_rate * 30
    epochs = [samples[i:i + w] for i in range(0, len(samples), w)]

    if len(epochs) != len(hypnograms[0]):
        print("RECORD {} epochs vs hypnogram_len".format(rec_folder))
        print(len(epochs))
        print(len(hypnograms[0]))
        print("Differ. FATAL ERROR.")
        exit()

    return np.array(epochs, dtype='float32'), np.array(hypnograms, dtype="int"), sample_rate
