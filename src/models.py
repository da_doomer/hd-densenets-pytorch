from torch import nn
import torch
import numpy as np

from tqdm import tqdm

from random import shuffle

def build_network(x_shape, classes_n):
    """shapes do not include batch dimension"""
    network = Network(x_shape, classes_n)
    return network

def train_network(
        x,
        y,
        network,
        optimizer,
        scheduler,
        loss_func,
        epochs,
        batch_size,
        ):
    """Returns network and loss per epoch list"""
    # Training settings
    losses = list()

    # Find if GPU is available
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    network = network.to(device)
    network.train()

    xy = list(zip(x, y))
    for epoch in tqdm(
            range(epochs),
            desc="Training on {}".format(str(device)),
            unit="epoch"):
        avgloss = 0
        avgacc = 0
        avgreg = 0
        curr = 1
        shuffle(xy)

        with tqdm(
                xy,
                total=len(xy),
                desc="Epoch {}".format(str(epoch)),
                unit="seq",
                ) as task_bar:
            for inp, target in task_bar:
                # Build target and input tensors
                inp = np.array(inp)
                target = np.array(target, dtype='long')
                inp = torch.from_numpy(inp)
                target = torch.from_numpy(target)

                # Send tensors to GPU (or CPU if CUDA unavailable)
                inp = inp.to(device)
                target = target.to(device)

                # Compute loss function
                optimizer.zero_grad()
                output = network(inp)
                loss = loss_func(output, target)

                # Step optimizer
                loss.backward()
                optimizer.step()

                # Step learning rate scheduler
                scheduler.step()

                # Batch-update bar
                avgloss = (avgloss*(curr-1) + loss.item())/curr
                acctensor = (target-output.argmax(dim=1)).bool().bitwise_not().to(torch.float16).mean()
                avgacc = (avgacc*(curr-1) + acctensor.item())/curr
                curr += 1

                task_bar.set_postfix(
                        avg_loss=avgloss,
                        avg_acc=avgacc,
                        avg_reg=avgreg,
                    )

        losses.append(avgloss)

    return network, losses

def network_classify(x, network, batch_size):
    # Find if GPU is available
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    network = network.to(device)
    network.eval()

    inp = torch.from_numpy(np.array(x)).to(device)
    with torch.no_grad():
        inp = nn.Softmax(dim=1)(network(inp))
    inp = inp.cpu().detach().numpy()
    inp = inp.argmax(axis=1)
    return inp

class Network(nn.Module):
    """Expects numpy data from a 1D multi-channel signal.
    Constructor specification:
        x_shape: (len, channels)
        y_shape: (classes_n,)

    Forward specification:
        x.shape: (n, len, channels)
        y.shape: (n, len, channels)

    Encode specification:
        x.shape: (n, len, channels)
        y.shape: (n, 64)

    Decode specification:
        x.shape: (n, 64)
        y.shape: (n, len, channels)
    """
    def __init__(self, x_shape, classes_n):
        super(Network,self).__init__()
        # Convolutions
        convs_n = 4
        channels = 2
        kernel_size = 4
        stride = 4
        pooling_kernel_size = 2
        branches_n = 50
        dropout_p = 0.2
        intermediate_size = 10
        intermediates_n = 1
        hidden_state_n = 10

        new_activation = lambda: nn.CELU()
        x = torch.zeros((100,*list(reversed(x_shape))))
        self.shapes = list()

        # Add branches
        new_conv_layer = lambda i: nn.Conv1d(
                in_channels= x_shape[-1] if i==0 else channels,
                out_channels=channels,
                kernel_size=kernel_size,
                stride=stride,
                )
        branches = list()
        for _ in tqdm(range(branches_n),desc="Adding branches to network"):
            # Add each branch
            branch = list()
            for i in range(convs_n):
                branch.append(new_conv_layer(i))
                branch.append(nn.MaxPool1d(kernel_size=pooling_kernel_size))
                branch.append(new_activation())
                #branch.append(nn.BatchNorm1d(channels))
                #branch.append(nn.AlphaDropout(p=dropout_p))
            branches.append(branch)
        self.branches = branches

        # Name branch layers and add as modules
        bxs = list()
        for i,branch in enumerate(self.branches):
            bx = x
            for j,l in enumerate(branch):
                l.requires_grad = False
                # Find shapes
                inshape = list(bx.shape)
                bx = l(bx)
                outshape = list(bx.shape)

                # Add module
                name = "Branch{}_{}".format(str(i),str(j))
                self.add_module(name,l)
                shapelabel = name + str(inshape) + " " + str(outshape)
                self.shapes.append(shapelabel)
            bxs.append(bx)

        # Concatenate branches
        x = torch.cat(bxs, dim=1)
        x = torch.flatten(x, start_dim=1)
        in_size = list(x.shape)[1]
        for _ in tqdm(range(in_size),desc="Counting conv neurons"):
            continue

        # Add intermediate layers
        self.intermediates = list()
        for i in range(intermediates_n):
            linear = nn.Linear(
                    in_features=in_size if i==0 else intermediate_size,
                    out_features=intermediate_size
                )
            self.intermediates.append(linear)
            self.intermediates.append(new_activation())

        # Name layers and add as modules
        for i,l in enumerate(self.intermediates):
            in_shape = list(x.shape)
            x = l(x)
            out_shape = list(x.shape)
            name = "dense{}".format(str(i))
            self.shapes.append(name + " " + str(in_shape) + " " + str(out_shape))
            self.add_module(name,l)

        # Add final layers
        self.finals = list()
        linear = nn.Linear(in_features=intermediate_size if intermediates_n > 0 else in_size, out_features=classes_n)
        self.finals.append(linear)

        # Name final layers and add as modules
        for i,l in enumerate(self.finals):
            in_shape = x.shape
            x = l(x)
            out_shape = x.shape
            name = "Layer{}".format(str(i))
            self.shapes.append(name + " " + str(in_shape) + " " + str(out_shape))
            self.add_module(name,l)

    def __str__(self):
        self.shapes = ["\nInput shape | Output shape\n"]+self.shapes
        return super(Network,self).__str__() + "\n".join(self.shapes)

    def forward(self, x):
        """(seq_len, len, channels)"""
        x = self.encode(x)
        for l in self.finals:
            x = l(x)
        return x

    def encode(self, x):
        x = x.transpose(2,1)
        # Pass through branches
        bxs = list()
        for branch in self.branches:
            bx = x
            for l in branch:
                bx = l(bx)
            bxs.append(bx)
        # Concatenate branches
        x = torch.cat(bxs, dim=1)
        x = torch.flatten(x, start_dim=1)
        for l in self.intermediates:
            x = l(x)
        return x
