from pathlib import Path
import random

report_filename = "report.pdf"

# ISRUC channel IDs
EOG_L  = ('LOC-A2', 'E1-M2', 'LOC')
EOG_R  = ('ROC-A1', 'E2-M1', 'ROC')
EEG_1  = ('F3-A2',  'F3-M2')
EEG_2  = ('C3-A2',  'C3-M2', 'C3')
EEG_3  = ('O1-A2',  'O1-M2')
EEG_4  = ('F4-A1',  'F4-M1')
EEG_5  = ('C4-A1',  'C4-M1')
EEG_5  = ('O2-A1',  'O2-M1')
CHIN   = ('X1','24',)
ECG    = ('X2','25',)
LEG_1  = ('X3','26',)
LEG_2  = ('X4','27',)
SNORE  = ('X5','28',)
FLOW_1 = ('X6','29',)
FLOW_2 = ('DC3', 'DC4', 'DC01')
ABDO_1 = ('X7','30')
ABDO_2 = ('X8','31')
OXYMET = ('SaO2','SpO2')
BODPOS = ('DC8',)
channels = [*EEG_2,*EOG_L]

database = Path("./dataset")
url = "http://dataset.isr.uc.pt/ISRUC_Sleep"

if not database.exists():
    print("Dataset does not exist. Will attempt to download and extract ISRUC-sleep data.")
    database.mkdir()
    subgroups = {"subgroupI" : 100, "subgroupII" : 8, "subgroupIII" : 10}
    for subgroup in subgroups: (database/subgroup).mkdir()
    tasks = [("{}/{}/{}.rar".format(url,sg,n+1), database/sg/"{}".format(n))\
            for sg in subgroups for n in range(subgroups[sg])]

    from tqdm import tqdm
    from urllib.request import urlopen
    from subprocess import run
    from tempfile import NamedTemporaryFile
    for (url, folder) in tqdm(tasks):
        folder.mkdir()
        with urlopen(url) as response, NamedTemporaryFile(delete=True,dir=folder) as outfile:
            outfile.write(response.read())
            run(["unrar","e", outfile.name], check=True, cwd=folder, capture_output=True)
    print("Dataset download and extraction successful.")

max_folders = 10
subgroups = {
        1 : "subgroupI",
        2 : "subgroupII",
        3 : "subgroupIII" ,
        0 : "*"
}
subgroup = subgroups[3]
rec_folders = set([str(Path(f)) for f in (database).glob("{}/*".format(subgroup))])
rec_folders = random.sample(rec_folders,min(max_folders,len(rec_folders)))

epochs = 32
batch_size = 4

context_left = 0
context_right = 0
