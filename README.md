# hd-randomnets-pytorch

Experiment of trying different degrees of randomness in neural
networks. The idea is to have a large number of shallow convolutional
"branches" (whose weights may be fixed) and train a classifier
over the encoded output.

The model is tested classifying sleep epochs 
on ISRUC-Sleep, a PSG dataset.

Instructions to run the script:

1. Make sure `unrar`, `python3.8` and `pipenv` are installed.
2. Clone this git repository.
3. `pipenv install`
4. `pipenv run python main.py`
